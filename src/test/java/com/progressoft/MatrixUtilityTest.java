package com.progressoft;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixUtilityTest {

    @Test
    public void givenMatricesWithIncompatibleDimensions_whenSum_thenThrowArrayIndexOutOfBoundsException() {
        int[][] first = {
                {1, 2},
                {1, 2},
                {1, 2}
        };

        int[][] second = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };
        Matrix firstMatrix = new Matrix(first);
        Matrix secondMatrix = new Matrix(second);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                                            () -> MatrixUtility.sum(firstMatrix, secondMatrix));
        Assertions.assertEquals("incompatible sizes", exception.getMessage());
    }

    @Test
    public void givenTwoValidMatrices_whenSum_thenReturnResult() {
        int[][] first = {
                {1, 2, 3},
                {4, 5, 6}
        };

        int[][] second = {
                {1, 2, 3},
                {4, 5, 6}
        };

        Matrix firstMatrix = new Matrix(first);
        Matrix secondMatrix = new Matrix(second);

        int[][] expected = {
                {2, 4, 6},
                {8, 10, 12}
        };

        int[][] result = MatrixUtility.sum(firstMatrix, secondMatrix);

        Assertions.assertArrayEquals(expected, result, "returned result not as expected");
    }

    @Test
    public void givenValidMatrix_whenTranspose_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 4, 7},
                {2, 5, 8},
                {3, 6, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        Assertions.assertArrayEquals(expected, MatrixUtility.transpose(matrix1));
    }
    @Test
    public void givenValidMatrix_whenScale_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int scalar = 2;
        Matrix matrix1 = new Matrix(matrix);
        int[][] expected = {
                {2, 4, 6},
                {8, 10, 12},
                {14, 16, 18}
        };
        Assertions.assertArrayEquals(expected, MatrixUtility.scale(matrix1, scalar));
    }

    @Test
    public void givenIncompatibleMatrices_whenMultiply_thenThrowException() {
        int[][] first = {
                {1, 2, 3},
                {4, 5, 6},
                {1, 1, 1},
                {1, 1, 1}
        };

        int[][] second = {
                {1, 2, 3},
                {4, 5, 6}
        };

        Matrix firstMatrix = new Matrix(first);
        Matrix secondMatrix = new Matrix(second);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtility.multiply(firstMatrix, secondMatrix));
        Assertions.assertEquals("incompatible matrices", exception.getMessage());
    }

    @Test
    public void givenValidMatrices_whenMultiply_thenReturnResult() {
        int[][] first = {
                {1, 2, 3},
                {4, 5, 6}
        };

        int[][] second = {
                {7, 8},
                {9, 10},
                {11, 12}
        };

        int[][] expected = {
                {58, 64},
                {139, 154}
        };

        Matrix firstMatrix = new Matrix(first);
        Matrix secondMatrix = new Matrix(second);

        int[][] result = MatrixUtility.multiply(firstMatrix, secondMatrix);

        Assertions.assertArrayEquals(expected, result, "returned result not as expected");
    }

    @Test
    public void givenInvalidRow_whenSubMatrix_thenThrowException() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtility.sub(matrix1, 7, 1));
        Assertions.assertEquals("Invalid row", exception.getMessage());
    }

    @Test
    public void givenInvalidColumn_whenSubMatrix_thenThrowException() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtility.sub(matrix1, 1, 7));
        Assertions.assertEquals("Invalid column", exception.getMessage());
    }

    @Test
    public void givenValidArguments_whenSubMatrix_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {4, 6},
                {7, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.sub(matrix1, 0, 1);
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    public void givenNotSquareMatrix_whenToDiagonal_thenThrowException() {
        int[][] matrix = {
                {1, 2, 3},
                {1, 2, 3}
        };

        Matrix matrix1 = new Matrix(matrix);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtility.toDiagonal(matrix1));
        Assertions.assertEquals("Matrix is not a square", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenToDiagonal_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 0, 0},
                {0, 5, 0},
                {0, 0, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toDiagonal(matrix1);
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    public void givenNotSquareMatrix_whenToUpperTriangle_thenThrowException() {
        int[][] matrix = {
                {1, 2, 3},
                {1, 2, 3}
        };

        Matrix matrix1 = new Matrix(matrix);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtility.toUpperTriangle(matrix1));
        Assertions.assertEquals("Matrix is not a square", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenToUpperTriangle_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 2, 3},
                {0, 5, 6},
                {0, 0, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toUpperTriangle(matrix1);
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    public void givenNotSquareMatrix_whenToLowerTriangle_thenThrowException() {
        int[][] matrix = {
                {1, 2, 3},
                {1, 2, 3}
        };

        Matrix matrix1 = new Matrix(matrix);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtility.toLowerTriangle(matrix1));
        Assertions.assertEquals("Matrix is not a square", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenToLowerTriangle_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 0, 0},
                {4, 5, 0},
                {7, 8, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toLowerTriangle(matrix1);
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    public void givenNotSquareMatrix_whenDeterminantOfTwoByTwo_thenThrowException() {
        int[][] matrix = {
                {1, 2, 3},
                {1, 2, 3}
        };

        Matrix matrix1 = new Matrix(matrix);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtility.determinantOfTwoByTwo(matrix1));
        Assertions.assertEquals("Matrix is not a square", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenDeterminantOfTwoByTwo_thenThrowException() {
        int[][] matrix = {
                {1, 2},
                {3, 4}
        };

        Matrix matrix1 = new Matrix(matrix);
        int expectedResult = -2;
        Assertions.assertEquals(expectedResult, MatrixUtility.determinantOfTwoByTwo(matrix1));
    }

    @Test
    public void givenValidMatrixAndLowerChoice_whenToTriangle_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 0, 0},
                {4, 5, 0},
                {7, 8, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toTriangleUsingEnum(matrix1, Choice.LOWER);

        Assertions.assertArrayEquals(expected,result);
    }

    @Test
    public void givenValidMatrixAndUpperChoice_whenToTriangle_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 2, 3},
                {0, 5, 6},
                {0, 0, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toTriangleUsingEnum(matrix1, Choice.UPPER);

        Assertions.assertArrayEquals(expected,result);
    }

    @Test
    public void givenValidMatrixAndDiagonalChoice_whenToTriangleUsingEnum_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 0, 0},
                {0, 5, 0},
                {0, 0, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toTriangleUsingEnum(matrix1, Choice.DIAGONAL);

        Assertions.assertArrayEquals(expected,result);
    }

    @Test
    public void givenValidMatrixAndDiagonalChoice_whenToTriangleUsingInterface_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 0, 0},
                {0, 5, 0},
                {0, 0, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toTriangleUsingInterface(matrix1, Choice.DIAGONAL);

        Assertions.assertArrayEquals(expected,result);
    }

    @Test
    public void givenValidMatrixAndUpperChoice_whenToTriangleUsingInterface_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 2, 3},
                {0, 5, 6},
                {0, 0, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toTriangleUsingInterface(matrix1, Choice.UPPER);

        Assertions.assertArrayEquals(expected,result);
    }

    @Test
    public void givenValidMatrixAndLowerChoice_whenToTriangleUsingInterface_thenReturnResult() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] expected = {
                {1, 0, 0},
                {4, 5, 0},
                {7, 8, 9}
        };

        Matrix matrix1 = new Matrix(matrix);
        int[][] result = MatrixUtility.toTriangleUsingInterface(matrix1, Choice.LOWER);

        Assertions.assertArrayEquals(expected,result);
    }
}




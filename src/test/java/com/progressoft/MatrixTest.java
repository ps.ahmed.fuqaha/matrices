package com.progressoft;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixTest {
    @Test
    public void givenInconsistentMatrix_whenConstructingAMatrix_thenThrowException() {
        int[][] matrix = {
                {1, 2, 3},
                {1, 2}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(matrix));
        Assertions.assertEquals("matrix is inconsistent", exception.getMessage());
    }

    @Test
    public void givenMatrixWithNullRow_whenConstructingAMatrix_thenThrowException() {
        int[][] matrix = {
                {1, 2, 3},
                {1, 2, 3},
                null
        };
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new Matrix(matrix));
        Assertions.assertEquals("matrix has a null row", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenConstructingAMatrix_thenCreateMatrix() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        Matrix matrix1 = new Matrix(matrix);
        matrix[2][2] = 3;
        Assertions.assertEquals(matrix1.getValue(2, 2), 9);
    }

    @Test
    public void givenMatrixWithDifferentDimensions_whenCheckEquality_ReturnFalse() {
        Matrix firstMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        Matrix secondMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {9, 8, 7}
        });

        Assertions.assertFalse(firstMatrix.equals(secondMatrix));
    }

    @Test
    public void givenNull_whenCheckEquality_ReturnFalse() {
        Matrix firstMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        Assertions.assertFalse(firstMatrix.equals(null));
    }

    @Test
    public void givenUnequalMatrices_whenCheckEquality_ReturnFalse() {
        Matrix firstMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 6}
        });
        Matrix secondMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {9, 8, 7}
        });

        Assertions.assertFalse(firstMatrix.equals(secondMatrix));
    }

    @Test
    public void givenDifferentType_whenCheckEquality_thenReturnFalse() {
        Matrix firstMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        String string = new String("hello");
        Assertions.assertFalse(firstMatrix.equals(string));
    }

    @Test
    public void givenEqualMatrices_whenCheckEquality_thenReturnTrue() {
        Matrix firstMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        Matrix secondMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });

        Assertions.assertTrue(firstMatrix.equals(secondMatrix), "Matrices are not equal");
        Assertions.assertEquals(firstMatrix.hashCode(), secondMatrix.hashCode(), "Equal matrices do not have the same hashcode");
        Assertions.assertFalse(firstMatrix.equals(null));
        Assertions.assertTrue(secondMatrix.equals(firstMatrix));
    }

    @Test
    public void givenUnequalMatrices_whenCheckEquality_thenReturnFalse() {
        Matrix firstMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        Matrix secondMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 9}
        });

        Assertions.assertFalse(firstMatrix.equals(secondMatrix), "should return false");
        Assertions.assertNotEquals(firstMatrix.hashCode(), secondMatrix.hashCode(), "Unequal matrices have the same hashcode");
        Assertions.assertFalse(secondMatrix.equals(firstMatrix));
    }

    @Test
    public void givenMatrix_whenPrintMatrix_ReturnDescription() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        Matrix matrix1 = new Matrix(matrix);
        String string = "Number of Rows: 3\nNumber of Columns: 3\n{1,2,3}\n{4,5,6}\n{7,8,9}\n";
        Assertions.assertEquals(string, matrix1.toString());
    }
}

package com.progressoft;

public class MatrixUtility {
    public static int[][] sum(Matrix firstMatrix, Matrix secondMatrix) {
        validateForSummation(firstMatrix, secondMatrix);
        return performSum(firstMatrix, secondMatrix);
    }

    public static int[][] scale(Matrix matrix, int scalar) {
        return performScaling(matrix, scalar);
    }

    public static int[][] transpose(Matrix matrix) {
        return performTransposing(matrix);
    }

    public static int[][] multiply(Matrix firstMatrix, Matrix secondMatrix) {
        validateForMultiplication(firstMatrix, secondMatrix);

        return performMultiplication(firstMatrix, secondMatrix);
    }

    public static int[][] sub(Matrix matrix, int removableRow, int removableColumn) {

        validateRow(matrix,removableRow);
        validateColumn(matrix,removableColumn);

        return getSub(matrix, removableRow, removableColumn);
    }

    public static int[][] toDiagonal(Matrix matrix) {
        failIfNotSquare(matrix);
        return getDiagonal(matrix);
    }

    public static int[][] toUpperTriangle(Matrix matrix) {
        failIfNotSquare(matrix);

        return getUpperTriangle(matrix);
    }

    public static int[][] toLowerTriangle(Matrix matrix) {
        failIfNotSquare(matrix);
        return getLowerTriangle(matrix);
    }

    public static int determinantOfTwoByTwo(Matrix matrix) {
        failIfNotSquare(matrix);

        return matrix.getValue(0,0) * matrix.getValue(1,1) - matrix.getValue(0,1) * matrix.getValue(1,0);

    }

    private static int[][] performSum(Matrix firstMatrix, Matrix secondMatrix) {
        int rows = firstMatrix.getRows();
        int columns = firstMatrix.getColumns();
        int[][] result = new int[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++)
                result[i][j] = firstMatrix.getValue(i,j) + secondMatrix.getValue(i,j);
        }
        return result;
    }

    private static void validateForSummation(Matrix firstMatrix, Matrix secondMatrix) {
        if (!hasSameDimensions(firstMatrix, secondMatrix))
            throw new IllegalArgumentException("incompatible sizes");
    }

    private static boolean hasSameDimensions(Matrix left, Matrix right) {
        return isSameRows(left, right) && isSameCols(left, right);
    }

    private static boolean isSameCols(Matrix left, Matrix right) {
        return left.getColumns() == right.getColumns();
    }

    private static boolean isSameRows(Matrix left,Matrix right) {
        return left.getRows() == right.getRows();
    }


    private static int[][] performScaling(Matrix matrix, int scalar) {
        int rows = matrix.getRows();
        int columns = matrix.getColumns();
        int[][] result = new int[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++)
                result[i][j] = matrix.getValue(i,j) * scalar;

        }
        return result;
    }


    private static int[][] performTransposing(Matrix matrix) {
        int rows = matrix.getRows();
        int columns = matrix.getColumns();
        int[][] result = new int[columns][rows];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++)
                result[j][i] = matrix.getValue(i,j);
        }

        return result;
    }

    private static int[][] performMultiplication(Matrix firstMatrix, Matrix secondMatrix) {
        int[][] result = new int[firstMatrix.getRows()][secondMatrix.getColumns()];

        for (int i = 0; i < firstMatrix.getRows(); i++) {
            for (int j = 0; j < secondMatrix.getColumns(); j++) {
                for (int k = 0; k < firstMatrix.getColumns(); k++) {
                    result[i][j] += firstMatrix.getValue(i,k) * secondMatrix.getValue(k,j);
                }
            }
        }


        return result;
    }

    private static void validateForMultiplication(Matrix firstMatrix, Matrix secondMatrix) {

        if (firstMatrix.getColumns() != secondMatrix.getRows())
            throw new IllegalArgumentException("incompatible matrices");

    }

    private static int[][] getSub(Matrix matrix, int removableRow, int removableColumn) {
        int rows = 0;

        int[][] result = new int[matrix.getRows() - 1][matrix.getColumns() - 1];
        for (int i = 0; i < matrix.getRows(); i++) {
            if (i == removableRow)
                continue;
            int columns = 0;
            for (int j = 0; j < matrix.getColumns(); j++)
                if (j != removableColumn)
                    result[rows][columns++] = matrix.getValue(i,j);
            rows++;
        }

        return result;
    }

    private static void validateColumn(Matrix matrix, int removableColumn) {
        if (matrix.getColumns() < removableColumn || removableColumn < 0)
            throw new IllegalArgumentException("Invalid column");
    }

    private static void validateRow(Matrix matrix, int removableRow) {
        if (removableRow > matrix.getRows() || removableRow < 0)
            throw new IllegalArgumentException("Invalid row");
    }


    private static int[][] getDiagonal(Matrix matrix) {
        int rows = matrix.getRows();
        int columns = matrix.getColumns();

        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++) {
                if (i != j)
                    result[i][j] = 0;
                else
                    result[i][j] = matrix.getValue(i,j);
            }
        return result;
    }


    private static int[][] getLowerTriangle(Matrix matrix) {

        int rows = matrix.getRows();
        int columns = matrix.getColumns();

        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++) {
                if (i < j)
                    result[i][j] = 0;
                else
                    result[i][j] = matrix.getValue(i,j);
            }
        return result;
    }

    private static int[][] getUpperTriangle(Matrix matrix) {

        int rows = matrix.getRows();
        int columns = matrix.getColumns();

        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++) {
                if (i > j)
                    result[i][j] = 0;
                else
                    result[i][j] = matrix.getValue(i,j);
            }

        return result;
    }

    private static boolean isSquare(Matrix matrix) {
        return matrix.getRows() == matrix.getColumns();
    }

    private static void failIfNotSquare(Matrix matrix) {
        if (!isSquare(matrix))
            throw new IllegalArgumentException("Matrix is not a square");
    }

    public static int[][] toTriangleUsingEnum(Matrix matrix, Choice choice) {

        int rows = matrix.getRows();
        int columns = matrix.getColumns();

        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++) {
                if (choice.checkCondition(i,j))
                    result[i][j] = 0;
                else
                    result[i][j] = matrix.getValue(i, j);
            }

        return result;
    }

    public static int[][] toTriangleUsingInterface(Matrix matrix, Choice choice) {

        int rows = matrix.getRows();
        int columns = matrix.getColumns();

        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++) {
                if (ChoiceFactory.choiceFactory(choice).checkCondition(i,j))
                    result[i][j] = 0;
                else
                    result[i][j] = matrix.getValue(i, j);
            }

        return result;
    }
}


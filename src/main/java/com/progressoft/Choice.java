package com.progressoft;

public enum Choice {
    UPPER,
    LOWER,
    DIAGONAL;

    public boolean checkCondition(int rows, int columns) {
        if (this == UPPER)
            return rows > columns;
        if (this == LOWER)
            return rows < columns;
        if (this == DIAGONAL)
            return rows != columns;

        throw new IllegalArgumentException("invalid type");
    }
}

package com.progressoft;

public class ChoiceFactory {

    public static IChoice choiceFactory(Choice choice) {

        switch (choice) {
            case LOWER: return ChoiceFactory::isLower;
            case UPPER: return ChoiceFactory::isUpper;
            case DIAGONAL: return ChoiceFactory::isDiagonal;
        }
        return  null;
    }

    private static boolean isLower(int rows, int columns) {
        return rows < columns;
    }

    private static boolean isDiagonal(int rows, int columns) {
        return rows != columns;
    }

    private static boolean isUpper(int rows, int columns) {
        return rows > columns;
    }
}

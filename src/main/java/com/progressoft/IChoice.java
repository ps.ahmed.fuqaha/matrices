package com.progressoft;

@FunctionalInterface
public interface IChoice {
    boolean checkCondition(int rows, int columns);
}

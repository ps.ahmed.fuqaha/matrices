package com.progressoft;

import java.util.Arrays;

public class Matrix {

    private final int[][] matrix;
    private final int rows;
    private final int columns;
    private final String description;
    private final int hash ;

    public Matrix(int[][] matrix) {
        String description1;
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i] == null)
                throw new NullPointerException("matrix has a null row");
            if (matrix[0].length != matrix[i].length)
                throw new IllegalArgumentException("matrix is inconsistent");
        }

        this.matrix = copyMatrix(matrix);
        this.rows = matrix.length;
        this.columns = matrix[0].length;
        description = buildDescription();

        hash = calculatehash();
    }

    private String buildDescription() {
        String description1;
        description1 = "Number of Rows: " + this.rows;
        description1 += "\nNumber of Columns: " + this.columns;
        description1 += "\n";
        for (int rows = 0; rows < this.rows; rows++) {
            description1 += "{";
            for (int columns = 0; columns < this.columns; columns++) {
                description1 += getValue(rows, columns);
                if (columns != this.columns - 1)
                    description1 += ",";
            }
            description1 += "}\n";
        }
        return description1;
    }

    private int[][] copyMatrix(int[][] matrix) {
        int[][] copy = new int[matrix.length][];
        for (int i = 0; i < matrix.length; i++)
            copy[i] = Arrays.copyOf(matrix[i], matrix[i].length);
        return copy;
    }

    public int getValue(int row, int column) {
        return matrix[row][column];
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;
        if (object.getClass() != this.getClass())
            return false;
        if (this.rows != ((Matrix) object).getRows() || this.columns != ((Matrix) object).getColumns())
            return false;
        for (int rows = 0; rows < this.rows; rows++)
            for (int columns = 0; columns < this.columns; columns++)
                if (matrix[rows][columns] != ((Matrix) object).getValue(rows, columns))
                    return false;

        return true;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    private int calculatehash() {
        int result = 17;
        result += 13 * result + rows;
        result += 13 * result + columns;
        result += Arrays.deepHashCode(matrix);
        return result;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
